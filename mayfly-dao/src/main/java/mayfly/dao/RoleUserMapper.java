package mayfly.dao;

import mayfly.dao.base.BaseMapper;
import mayfly.entity.RoleUser;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2019-08-19 20:13
 */
public interface RoleUserMapper extends BaseMapper<RoleUser> {
}
