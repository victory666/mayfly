package mayfly.dao;

import mayfly.dao.base.BaseMapper;
import mayfly.entity.Role;

/**
 * @Description: 角色Mapper
 * @author: hml
 * @date: 2018/6/27 下午2:37
 */
public interface RoleMapper extends BaseMapper<Role> {
}
