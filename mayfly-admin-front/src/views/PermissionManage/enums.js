import {Enum} from '~/common/Enum.js'

/**
 * 枚举类
 */
export default {
  // 资源类型枚举
  ResourceTypeEnum: new Enum().add('MENU', '菜单', 1).add('PERMISSION', '权限', 2)
}